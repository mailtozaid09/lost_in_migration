import React,{useState} from 'react'
import {Text, View, SafeAreaView, Image, Dimensions, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import { how_to_play_data } from '../../global/data'
import { fontSize } from '../../global/fontFamily'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { media } from '../../global/media'

const StartScreen = ({navigation}) => {

    const [currentId, setCurrentId] = useState(1);

    const arrowFunction = (val) => {
        if(val == 'left'){
            setCurrentId(prev => prev-1)
        }else{
            setCurrentId(prev => prev+1)
        }
    }
    return (
        <SafeAreaView style={styles.container} >
            <Image source={media.help1} style={{height: 300, }} />
            
            <View style={{alignItems: 'center'}} >
                <Text style={{fontSize: 34, fontWeight: 'bold', color: 'white'}} >Lost In Migation</Text>
                <Text style={{fontSize: 24, marginTop: 10, marginBottom: 40, fontWeight: 'bold', color: 'white'}}>Attention</Text>
            </View>

          
            
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%'}} >
                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => {navigation.navigate('HowToPlay')}}
                    style={{height: 60, flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#00000080'}} >
                    <Text style={{fontSize: fontSize.Title, fontWeight: 'bold', color: colors.primary}} >PLAY</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.primary,
        width: screenWidth,
    },
    mainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
    },
    cardContainer: {
        backgroundColor: '#00000050',
        padding: 30,
        margin: 30,
        flex: 1,
        alignItems: 'center', 
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 100,
        width: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#00000080',
    },
    title: {
        fontSize: fontSize.SubHeading,
        fontWeight: '500',
        color: colors.white,
        marginBottom: 10,
    },
    description: {
        fontSize: fontSize.Title,
        fontWeight: '400',
        color: colors.white,
    },
})

export default StartScreen