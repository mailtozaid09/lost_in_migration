import React,{useState, useEffect}  from 'react'
import {Text, View, StyleSheet, Dimensions, Image, SafeAreaView, Alert,  } from 'react-native'
import { media } from '../../global/media'
import { migration_data } from '../../global/data'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import GestureComponent from '../../components/gesture/GestureComponent'
import ResponseModal from '../../components/modal/ResponseModal'

const HomeScreen = () => {

    const [grid, setGrid] = useState([]);
    const [questions, setQuestions] = useState(null);

    const [questionText, setQuestionText] = useState('');
    const [questionImg, setQuestionImg] = useState('');


    const [correctAnswer, setCorrectAnswer] = useState('');
    const [questionCount, setQuestionCount] = useState(0);

    const [isAnswerCorrect, setIsAnswerCorrect] = useState(false);

    const [showResponseModal, setShowResponseModal] = useState(false);
 

    useEffect(() => {
        
        console.log("migration_data ", migration_data);

        responseDetails()
      
    }, [questionCount])

    const responseDetails = () => {
        if(questionCount == questions?.length){
            console.log('====================================');
            console.log("done");
            console.log('====================================');
            
            Alert.alert('GAME OVER')
        }else{
        
            console.log('====================================');
            console.log("increase");
            console.log('====================================');

           
            console.log('====================================');
            console.log("questionCount => ", questionCount);
            console.log('====================================');
            setGrid(migration_data.game_data.questions[questionCount].options.grid)
            setQuestions(migration_data.game_data.questions)
            setQuestionText(migration_data.game_data.questions[questionCount].statement)
            setCorrectAnswer(migration_data.game_data.questions[questionCount].correctAnswer)

        }  

    
    }

    const showResponseAnimation = () => {
        setShowResponseModal(true)

        setTimeout(() => {
            setShowResponseModal(false)

            
        }, 1500);
    }

    const checkAnswer = (value) => {
        setQuestionCount(prev => prev + 1)

        if(value == correctAnswer){
            setIsAnswerCorrect(true)
            //console.log("Answer Is Correct");
        }else {
            //console.log("Answer Is Wrong");
            setIsAnswerCorrect(false)
        }

        showResponseAnimation()

        console.log('====================================');
        console.log("questionCount ", questionCount +"  " + questions.length);
        console.log('====================================');

        

            
    }



    const RenderGridTile = () => {   
        let tiles = [];
    
        grid.forEach((row, i) => {
          let rows = row.forEach((e, j) => {
            tiles.push(
                    <View>
                        <Image
                            //source={media.bird}
                            source={grid[i][j].answer && media.bird}
                            style={[grid[i][j].answer ? {transform: [{rotate: grid[i][j].rotation}],} : null, { height: screenWidth/5-10, width: screenWidth/5-10 }]}
                        />
                    </View>
                );
            });
        });
        return tiles;
    }
    
    return (
        <SafeAreaView style={styles.container} >
            <GestureComponent
                correctAnswer={correctAnswer}
                checkAnswer={(value) => {checkAnswer(value)}}
            >
                <View style={{justifyContent: 'space-evenly', flex: 1, alignItems: 'center'}} >
                    <Text style={{fontSize: 18, textAlign: 'center'}} >{questionText}</Text>
                    <View style={{ alignItems: 'center', justifyContent: 'center',  width: screenWidth-40, flexDirection: 'row', flexWrap: 'wrap'}} >
                        <RenderGridTile />
                    </View>

                    
                </View> 

                {showResponseModal && (
                    <ResponseModal 
                        isAnswerCorrect={isAnswerCorrect}
                        showResponseModal={showResponseModal} 
                    />
                )}
               
            </GestureComponent>

            
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.primary
    }
})

export default HomeScreen