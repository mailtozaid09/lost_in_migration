import React,{useState} from 'react'
import {Text, View, SafeAreaView, Image, Dimensions, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import { how_to_play_data } from '../../global/data'
import { fontSize } from '../../global/fontFamily'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { media } from '../../global/media'

const HowToPlay = ({navigation}) => {

    const [currentId, setCurrentId] = useState(1);

    const arrowFunction = (val) => {
        if(val == 'left'){
            setCurrentId(prev => prev-1)
        }else{
            setCurrentId(prev => prev+1)
        }
    }
    return (
        <SafeAreaView style={styles.container} >
            <Text></Text>

            <View style={styles.mainContainer} >
                {currentId > 1 
                ?
                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => {arrowFunction('left')}}
                    style={styles.buttonContainer} >
                    <Image source={media.left_arrow} style={{height: 20, width: 20, marginRight: 4}} />
                </TouchableOpacity>
                : 
                <View style={[styles.buttonContainer, {backgroundColor: 'transparent'}]} />
                }


                <View style={styles.cardContainer} >
                
                    {how_to_play_data.map((item) => (
                        <>
                            {item.id == currentId
                            ?
                                <View style={{alignItems: 'center'}} >
                                    <Text style={styles.title} >How To Play</Text>

                                    <Text style={styles.description} >{item.id}. {item.title}</Text>

                                    <Image source={item.image} style={{height: 240, width: 240}} />
                                </View>
                            : 
                                null
                            }
                        </>
                    ))}

                    <View style={{flexDirection: 'row', alignItems: 'center'}} >
                        {how_to_play_data.map((item) => (
                            <View style={{height: 10, width: 10, borderRadius: 5, marginRight: 8, backgroundColor: item.id == currentId ? 'white' : '#ffffff80'}} />
                        ))}
                    </View>
                </View>
                {currentId < 3 ? <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => {arrowFunction('right')}}
                    style={styles.buttonContainer} >
                    <Image source={media.left_arrow} style={{height: 20, width: 20, marginLeft: 8, transform: [{ rotate: '180deg'}]}} />
                </TouchableOpacity>
                :
                    <View style={[styles.buttonContainer, {backgroundColor: 'transparent'}]} />
                }
            </View>
            
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%'}} >
                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => {navigation.goBack()}}
                    style={{height: 60, width: 60, alignItems: 'center', justifyContent: 'center', backgroundColor: '#00000080'}} >
                    <Image source={media.close} style={{height: 20, width: 20, }} />
                </TouchableOpacity>
                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => {navigation.navigate('HomeScreen')}}
                    style={{height: 60, flex: 1, marginLeft: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: '#00000080'}} >
                    <Text style={{fontSize: fontSize.Title, fontWeight: 'bold', color: colors.primary}} >PLAY</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.primary,
        width: screenWidth,
    },
    mainContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
    },
    cardContainer: {
        backgroundColor: '#00000050',
        padding: 30,
        margin: 30,
        flex: 1,
        alignItems: 'center', 
        justifyContent: 'center'
    },
    buttonContainer: {
        height: 100,
        width: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#00000080',
    },
    title: {
        fontSize: fontSize.SubHeading,
        fontWeight: '500',
        color: colors.white,
        marginBottom: 10,
    },
    description: {
        fontSize: fontSize.Title,
        fontWeight: '400',
        color: colors.white,
    },
})

export default HowToPlay