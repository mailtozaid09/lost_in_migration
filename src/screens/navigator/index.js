import React, {useState, useEffect} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../home/HomeScreen';
import RoughScreen from '../RoughScreen';
import HowToPlay from '../help/HowToPlay';
import StartScreen from '../onboarding/StartScreen';


const Stack = createStackNavigator();


const Navigator = ({navgation}) => {

    return (
        <Stack.Navigator initialRouteName='StartScreen'>
            <Stack.Screen
                name="StartScreen"
                component={StartScreen}
                options={{
                    headerShown: false
                }}
            /> 
            <Stack.Screen
                name="HomeScreen"
                component={HomeScreen}
                options={{
                    headerShown: false
                }}
            /> 
            <Stack.Screen
                name="HowToPlay"
                component={HowToPlay}
                options={{
                    headerShown: false
                }}
            /> 
            <Stack.Screen
                name="RoughScreen"
                component={RoughScreen}
                options={{
                    headerShown: false
                }}
            /> 
        </Stack.Navigator>
    );
}

export default Navigator