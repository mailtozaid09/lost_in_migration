import React from 'react'
import {Button, SafeAreaView, Text, View,StyleSheet} from 'react-native'
import { useState } from 'react';


const RoughScreen = () => {

    const [counter, setCounter] = useState(0);

    const incrementFunc = () => {
        console.log("inc +");

        var abc = counter
        abc++
        console.log(abc);

        setCounter(abc)
    }

    const decrementFunc = () => {
        console.log("dec +");
        var abc = counter
    
        console.log(abc);

        if(abc>0){
            abc--
            setCounter(abc)
        }

        
    }


    return (
        <SafeAreaView>
            <View style={styles.container}>
                <Button 
                    title='inc +'
                    onPress={() => {incrementFunc()}}
                />
                <Button 
                    title='dec -'
                    onPress={() => {decrementFunc()}}
                />

                
                <Text>{counter}</Text>
            </View>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
container:{
    width: '100%',
      height: '30%',
      justifyContent: 'center',
      alignItems: 'center'

}

})
export default RoughScreen