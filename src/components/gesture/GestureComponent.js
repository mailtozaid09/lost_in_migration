import React, { useState } from "react";
import { View, Text } from "react-native";
import GestureRecognizer, { swipeDirections } from "react-native-swipe-detect";
import { screenHeight, screenWidth } from "../../global/constants";
import ResponseModal from "../modal/ResponseModal";
import { colors } from "../../global/colors";


const GestureComponent = (props) => {

    const { children, correctAnswer, checkAnswer, } = props;

    const [myText, setMyText] = useState("I'm ready to get swiped!");
    const [gestureName, setGestureName] = useState("none");
    
    
    const [backgroundColor, setBackgroundColor] = useState("#fff");

    const [isAnswerCorrect, setIsAnswerCorrect] = useState(false);

    const onSwipeUp = (gestureState) => {
        //console.log("GestureState : ", 'You swiped up!');
        setMyText("You swiped up!")
    }

    const onSwipeDown = (gestureState) => {
        //console.log("GestureState : ", 'You swiped down!');
        setMyText("You swiped down!")
    }

    const onSwipeLeft = (gestureState) => {
        //console.log("GestureState : ", 'You swiped left!');
        setMyText("You swiped left!")
    }

    const onSwipeRight = (gestureState) => {
        //console.log("GestureState : ", 'You swiped right!');
        setMyText("You swiped right!")
    }

    const onSwipe = (gestureName, gestureState) => {

        const { SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT } = swipeDirections;
        
        setGestureName(gestureName)


        switch (gestureName) {
        case SWIPE_UP:
            checkAnswer(SWIPE_UP)
            // if(correctAnswer == 'SWIPE_UP'){
            //     setIsAnswerCorrect(true);
            //     setBackgroundColor('green');
            // }else{
            //     setIsAnswerCorrect(false);
            //     setBackgroundColor('red');
            // }
            break;
        case SWIPE_DOWN:
            checkAnswer(SWIPE_DOWN)
            // if(correctAnswer == 'SWIPE_DOWN'){
            //     setIsAnswerCorrect(true);
            //     setBackgroundColor('green');
            // }else{
            //     setIsAnswerCorrect(false);
            //     setBackgroundColor('red');
            // }
            break;
        case SWIPE_LEFT:
            checkAnswer(SWIPE_LEFT)
            // if(correctAnswer == 'SWIPE_LEFT'){
            //     setIsAnswerCorrect(true);
            //     setBackgroundColor('green');
            // }else{
            //     setIsAnswerCorrect(false);
            //     setBackgroundColor('red');
            // }
            break;
        case SWIPE_RIGHT:
            checkAnswer(SWIPE_RIGHT)
            // if(correctAnswer == 'SWIPE_RIGHT'){
            //     setIsAnswerCorrect(true);
            //     setBackgroundColor('green');
            // }else{
            //     setIsAnswerCorrect(false);
            //     setBackgroundColor('red');
            // }
            break;
        }
    }

    const config = {
        velocityThreshold: 0.3,
        directionalOffsetThreshold: 80,
    };

    return (
        <GestureRecognizer
            onSwipe={(direction, state) => onSwipe(direction, state)}
            onSwipeUp={(state) => onSwipeUp(state)}
            onSwipeDown={(state) => onSwipeDown(state)}
            onSwipeLeft={(state) => onSwipeLeft(state)}
            onSwipeRight={(state) => onSwipeRight(state)}
            config={config}
            style={{
            flex: 1,
            height: screenHeight,
            backgroundColor: colors.primary
            }}
        >
            <View style={{ width: screenWidth, flex: 1, alignItems: 'center', justifyContent: 'center'}} >
                {/* <Text>{(isAnswerCorrect).toString()}</Text> */}
                {children}
             {/* <Text>{myText}</Text>
            <Text>onSwipe callback received gesture: {gestureName}</Text> */}
            </View>

            
        </GestureRecognizer>
    );    
}

export default GestureComponent;
