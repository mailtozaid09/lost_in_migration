import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, FlatList, Modal, Pressable, SafeAreaView, ScrollView, } from 'react-native';
import { screenHeight } from '../../global/constants';
import { media } from '../../global/media';

const ResponseModal = ({showResponseModal, closeResponseModal, isAnswerCorrect}) => {

    return (
        <SafeAreaView style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={showResponseModal}
                onRequestClose={closeResponseModal}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#00000090'}}>
                    <Image source={isAnswerCorrect ? media.correct : media.incorrect} style={{height: 200, width: 200}} />
                </View>
            </Modal>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      height: screenHeight,
      //justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute'
    },

  });


export default ResponseModal