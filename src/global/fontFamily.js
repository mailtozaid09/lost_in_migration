export const Poppins = {
    Black: 'Poppins-Black',
    Bold: 'Poppins-Bold',
    ExtraBold: 'Poppins-ExtraBold',
    ExtraLight: 'Poppins-ExtraLight',
    Light: 'Poppins-Light',
    Medium: 'Poppins-Medium',
    Regular: 'Poppins-Regular',
    SemiBold: 'Poppins-SemiBold',
    Thin: 'Poppins-Thin',
};
  

export const fontSize = {
  Heading: 30,
  SubHeading: 24,
  Title: 20,
  SubTitle: 16,
  Body: 14,
}