export const media = {
    bird: require('../assets/bird.png'),

    help1: require('../assets/help1.png'),
    help2: require('../assets/help2.png'),
    help3: require('../assets/help3.png'),

    correct: require('../assets/correct.png'),
    incorrect: require('../assets/incorrect.png'),

    left_arrow: require('../assets/left-arrow.png'),

    close: require('../assets/close.png'),

    
}