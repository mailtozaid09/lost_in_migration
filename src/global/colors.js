export const colors = {
    primary: '#62c6de',


    dark_blue: '#212c46',
    
    black: '#252a34',
    white: '#fdfdfd',
    
    gray: '#919399',
    light_gray: '#ebecf0',
    
    blue: '#81b3f3',
    red: '#f29999',
    orange: '#f7c191',
    reddish: '#ED4545',
}