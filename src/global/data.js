import { colors } from "./colors";
import { media } from "./media";

export const sampleData = [
    {
        id: 1
    },
    {
        id: 2
    },
]

export const how_to_play_data = [
    {
        id: 1,
        title: 'A flock of birds will appear on the screen.',
        image: media.help1,
    },
    {
        id: 2,
        title: 'Swipe in the direction the middle bird is facing.',
        image: media.help2,
    },
    {
        id: 3,
        title: "Don't let the direction of the other birds distract you.",
        image: media.help3,
    },
]


export const migration_data = {
    "name": "Lost In Migration",
    "category": "Attention",
    "current_level": 1,
    "game_data": {
        "time": 10,
        "score_per_question": 10,
        "questions": [
            {
                "statement": "Swipe in direction of the middle bird",
                "correctAnswer": 'SWIPE_DOWN',
                "options": {
                    "grid": [
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '0deg',
                                "answer": true,
                            },
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '0deg',
                                "answer": true,
                            },
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '90deg',
                                "answer": true,
                            },
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '0deg',
                                "answer": true,
                            },
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '0deg',
                                "answer": true,
                            },
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                    ]
                }
            },
            {
                "statement": "Swipe in direction of the middle bird",
                "correctAnswer": 'SWIPE_LEFT',
                "options": {
                    "grid": [
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '0deg',
                                "answer": true,
                            },
                            {},
                            {
                                "shape": "bird",
                                "rotation": '0deg',
                                "answer": true,
                            },
                        ],
                        [
                            {},
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '180deg',
                                "answer": true,
                            },
                            {},
                        ],
                        [
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '0deg',
                                "answer": true,
                            },
                            {},
                            {
                                "shape": "bird",
                                "rotation": '0deg',
                                "answer": true,
                            },
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                    ]
                }
            },
            {
                "statement": "Swipe in direction of the middle bird",
                "correctAnswer": 'SWIPE_RIGHT',
                "options": {
                    "grid": [
                        [
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '180deg',
                                "answer": true,
                            },
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '90deg',
                                "answer": true,
                            },
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '0deg',
                                "answer": true,
                            },
                        ],
                        [
                            {},
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '270deg',
                                "answer": true,
                            },
                            {},
                        ],
                        [
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '180deg',
                                "answer": true,
                            },
                            {},
                            {},
                        ],
                        
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                    ]
                }
            },
            {
                "statement": "Swipe in direction of the middle bird",
                "correctAnswer": 'SWIPE_LEFT',
                "options": {
                    "grid": [
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '0deg',
                                "answer": true,
                            },
                            {},
                            {},
                        ],
                        [
                            {},
                            {
                                "shape": "bird",
                                "rotation": '90deg',
                                "answer": true,
                            },
                            {
                                "shape": "bird",
                                "rotation": '180deg',
                                "answer": true,
                            },
                            {
                                "shape": "bird",
                                "rotation": '270deg',
                                "answer": true,
                            },
                            
                            {},
                        ],
                        [
                            {},
                            {},
                            {
                                "shape": "bird",
                                "rotation": '90deg',
                                "answer": true,
                            },
                            {},
                            {},
                        ],
                        
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                        [
                            {},
                            {},
                            {},
                            {},
                            {},
                        ],
                    ]
                }
            },
        ]
    }
}