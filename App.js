import React, {useState, useEffect} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, LogBox, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import Navigator from './src/screens/navigator';


const App = ({navgation}) => {

    LogBox.ignoreAllLogs(true)

    return (
        <NavigationContainer>
            <Navigator />
        </NavigationContainer>
    );
}

export default App